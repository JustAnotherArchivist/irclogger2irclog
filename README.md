Script to transform the old EFnet irclogger logs into the new irclog format. You do not want to ever have to use this. Go away while you still can. Nothing good lies this way.

```bash
for d in *; do echo "$d"; for month in $(ls -1 "${d}"/* | sed 's,^.*/,,; s/-[0-9][0-9],.*$//' | uniq); do echo "$month"; { for f in "${d}/${month}"-*; do python3 efnet-irclogger-convert.py "$f"; done; } > ../out/efnet_${d}/${month}.log; done; done |& tee ../conversion.log
```

License: WTFPL
